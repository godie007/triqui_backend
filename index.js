const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
app.use(bodyParser.json());
app.use(cors());

let DB =[];

app.get('/states', (req, res) => {
  res.send(DB)
});

app.post('/states', (req, res) => {
  const states = req.body;
  console.log(JSON.stringify(states));
  DB = states;
  DB.player = states.player === 1 ? 2 : 1;
});

app.listen(3001);
